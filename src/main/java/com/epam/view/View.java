package com.epam.view;

import com.epam.Application;
import com.epam.model.parsers.GsonExample;
import com.epam.model.parsers.JacksonExample;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", ("1 - Jackson"));
        menu.put("2", ("2 - Gson"));
        menu.put("Q", ("Q - Exit"));
    }

    public View() {
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("Q", this::pressButton3);


    }

    private void pressButton1() throws IOException {
        JacksonExample j = new JacksonExample();
        j.buildJackson();
    }

    private void pressButton2() throws FileNotFoundException {
        GsonExample g = new GsonExample();
        g.buildGson();
    }

    private void pressButton3() {
        logger1.info("Bye-Bye");
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nJSON:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


