package com.epam.view;

/**
 * Interface to connect with controller.
 * Created by Borys Latyk on 13/11/2019.
 * @version 2.1
 * @since 10.11.2019
 */
@FunctionalInterface
public interface Printable {
    void print() throws Exception;
}
