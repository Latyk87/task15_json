package com.epam.model.parsers;
import com.epam.Application;
import com.epam.model.Plane;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class shows example of Jackson project.
 * Created by Borys Latyk on 15/12/2019.
 *
 * @version 2.1
 * @since 15.12.2019
 */
public class JacksonExample {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private ObjectMapper objectMapper;

    public JacksonExample() {
        this.objectMapper = new ObjectMapper();
    }

    public void buildJackson() throws IOException {
        File json = new File("E:/Epam knowledge/myjson/src/main/resources/json/planesJSON.json");

        List<Plane> p = createPlane(json);
        Comparator<Plane> planeComparator = (o1, o2) -> o2.getPrice() - o1.getPrice();
        Collections.sort(p, planeComparator);
        for (Plane p1 : p) {
            logger1.info(p1);
        }

    }

    public List<Plane> createPlane(File json1) throws IOException {
        Plane[] planes;
        planes = objectMapper.readValue(json1, Plane[].class);
        return Arrays.asList(planes);
    }
}
