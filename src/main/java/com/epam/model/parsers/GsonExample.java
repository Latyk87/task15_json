package com.epam.model.parsers;

import com.epam.Application;
import com.epam.model.Plane;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class shows example Google’s GSON.
 * Created by Borys Latyk on 15/12/2019.
 *
 * @version 2.1
 * @since 15.12.2019
 */
public class GsonExample {
    private Gson gson;
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public GsonExample() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    public void buildGson() throws FileNotFoundException {
        File json = new File("E:/Epam knowledge/myjson/src/main/resources/json/planesJSON.json");

        List<Plane> p2 = createPlaneGson(json);
        Comparator<Plane> planeComparator2 = (o1, o2) -> (int) (o2.getLength() - o1.getLength());
        Collections.sort(p2, planeComparator2);
        for (Plane pg : p2) {
            logger1.info(pg);

        }
    }

    public List<Plane> createPlaneGson(File json2) throws FileNotFoundException {
        Plane[] planes2;
        planes2 = gson.fromJson(new FileReader(json2), Plane[].class);

        return Arrays.asList(planes2);
    }


}
